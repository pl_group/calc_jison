/* parser generated by jison 0.4.13 */
/*
  Returns a Parser object of the following structure:

  Parser: {
    yy: {}
  }

  Parser.prototype: {
    yy: {},
    trace: function(),
    symbols_: {associative list: name ==> number},
    terminals_: {associative list: number ==> name},
    productions_: [...],
    performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate, $$, _$),
    table: [...],
    defaultActions: {...},
    parseError: function(str, hash),
    parse: function(input),

    lexer: {
        EOF: 1,
        parseError: function(str, hash),
        setInput: function(input),
        input: function(),
        unput: function(str),
        more: function(),
        less: function(n),
        pastInput: function(),
        upcomingInput: function(),
        showPosition: function(),
        test_match: function(regex_match_array, rule_index),
        next: function(),
        lex: function(),
        begin: function(condition),
        popState: function(),
        _currentRules: function(),
        topState: function(),
        pushState: function(condition),

        options: {
            ranges: boolean           (optional: true ==> token location info will include a .range[] member)
            flex: boolean             (optional: true ==> flex-like lexing behaviour where the rules are tested exhaustively to find the longest match)
            backtrack_lexer: boolean  (optional: true ==> lexer regexes are tested in order and for each matching regex the action code is invoked; the lexer terminates the scan when a token is returned by the action code)
        },

        performAction: function(yy, yy_, $avoiding_name_collisions, YY_START),
        rules: [...],
        conditions: {associative list: name ==> set},
    }
  }


  token location info (@$, _$, etc.): {
    first_line: n,
    last_line: n,
    first_column: n,
    last_column: n,
    range: [start_number, end_number]       (where the numbers are indexes into the input string, regular zero-based)
  }


  the parseError function receives a 'hash' object with these members for lexer and parser errors: {
    text:        (matched text)
    token:       (the produced terminal token, if any)
    line:        (yylineno)
  }
  while parser (grammar) errors will also provide these members, i.e. parser errors deliver a superset of attributes: {
    loc:         (yylloc)
    expected:    (string describing the set of expected tokens)
    recoverable: (boolean: TRUE when the parser has a error recovery rule available for this particular error)
  }
*/
var calculator = (function(){
var parser = {trace: function trace() { },
yy: {},
symbols_: {"error":2,"program":3,"block":4,"EOF":5,"cons":6,"vari":7,"proc":8,"statements":9,"expressions":10,"PROCEDURE":11,"ID":12,";":13,"CONST":14,"constlist":15,"=":16,"NUMBER":17,",":18,"VAR":19,"varlist":20,"statement":21,"CALL":22,"BEGIN":23,"END":24,"IF":25,"condition":26,"THEN":27,"WHILE":28,"codition":29,"DO":30,"ODD":31,"comparision":32,"==":33,"#":34,"<":35,"<=":36,">":37,">=":38,"compidexp":39,"s":40,"e":41,"PI":42,"E":43,"+":44,"-":45,"*":46,"/":47,"^":48,"!":49,"%":50,"(":51,")":52,"$accept":0,"$end":1},
terminals_: {2:"error",5:"EOF",11:"PROCEDURE",12:"ID",13:";",14:"CONST",16:"=",17:"NUMBER",18:",",19:"VAR",22:"CALL",23:"BEGIN",24:"END",25:"IF",27:"THEN",28:"WHILE",29:"codition",30:"DO",31:"ODD",33:"==",34:"#",35:"<",36:"<=",37:">",38:">=",42:"PI",43:"E",44:"+",45:"-",46:"*",47:"/",48:"^",49:"!",50:"%",51:"(",52:")"},
productions_: [0,[3,2],[4,0],[4,2],[4,2],[4,2],[4,2],[4,1],[8,0],[8,5],[8,1],[6,0],[6,4],[15,5],[15,3],[15,1],[7,0],[7,3],[20,3],[20,1],[20,1],[21,3],[21,2],[21,3],[21,4],[21,4],[9,2],[9,1],[9,1],[26,2],[26,1],[32,3],[32,3],[32,3],[32,3],[32,3],[32,3],[32,1],[39,3],[39,3],[39,3],[39,3],[39,3],[39,3],[10,1],[10,3],[40,0],[40,1],[41,3],[41,3],[41,3],[41,3],[41,3],[41,3],[41,3],[41,3],[41,2],[41,2],[41,2],[41,3],[41,1],[41,1],[41,1],[41,1]],
performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate /* action[1] */, $$ /* vstack */, _$ /* lstack */) {
/* this == yyval */

var $0 = $$.length - 1;
switch (yystate) {
case 1: 
          this.$ = $$[$0-1]; 
          console.log(this.$);
          return [this.$, symbol_table];
        
break;
case 3:
            if ($$[$0]) this.$ = [$$[$0-1], $$[$0]];
            else this.$ = $$[$0-1];
            //this.$ = $$[$0-1];
            //this.$.push($$[$0]);
         
break;
case 4:
            if ($$[$0]) this.$ = [$$[$0-1], $$[$0]];  //this.$ = $$[$0-1]; this.$.push($$[$0])
            else this.$ = $$[$0-1];
         
break;
case 5:
            if ($$[$0]) this.$ = [$$[$0-1], $$[$0]];  //this.$ = $$[$0-1]; this.$.push($$[$0])
            else this.$ = $$[$0-1];
         
break;
case 6:
            if ($$[$0]) this.$ = [$$[$0-1], $$[$0]];  //this.$ = $$[$0-1]; this.$.push($$[$0])
            else this.$ = $$[$0-1];
         
break;
case 7:this.$ = $$[$0];
break;
case 9:this.$ = [$$[$0-3], $$[$0-1]]
break;
case 10:this.$ = $$[$0]
break;
case 12:
            if ($$[$0]) this.$ = [$$[$0-2], $$[$0]]; //this.$ = $$[$0-2]; this.$.push($$[$0])
            else this.$ = $$[$0-2];
         
break;
case 13:
            if ($$[$0]) this.$ = [$$[$0-2], $$[$0]]; //this.$ = $$[$0-2]; this.$.push($$[$0])
            else this.$ = $$[$0-2];
            symbol_table[$$[$0-4]] = Number($$[$0-2]);
         
break;
case 14:
            this.$ = $$[$0];
            symbol_table[$$[$0-2]] = Number($$[$0]);
         
break;
case 15:this.$ = $$[$0];
break;
case 17:this.$ = $$[$0-1];
break;
case 18:
          if ($$[$0]) this.$ = [$$[$0-2], $$[$0]]; //this.$ = $$[$0-2]; this.$.push($$[$0])
          else this.$ = $$[$0-2];
       
break;
case 19:this.$ = $$[$0];
break;
case 20:this.$ = $$[$0];
break;
case 21:
             this.$ = [$$[$0-2], $$[$0]];
             symbol_table[$$[$0-2]] = $$[$0];
          
break;
case 22:this.$ = $$[$0-1]; symbol_table[$$[$0-1]] = $$[$0]
break;
case 23:this.$ = $$[$0-2]; symbol_table[$$[$0-2]] = $$[$0-1]
break;
case 24:
            this.$ = [$$[$0-2], $$[$0]];
            symbol_table[$$[$0-3]] = $$[$0-2];
            symbol_table[$$[$0-1]] = $$[$0];
         
break;
case 25:
            this.$ = [$$[$0-2], $$[$0]];
            symbol_table[$$[$0-3]] = $$[$0-2];
            symbol_table[$$[$0-1]] = $$[$0];
         
break;
case 26:this.$ = $$[$0-1];
break;
case 27:this.$ = $$[$0];
break;
case 28:this.$ = $$[$0];
break;
case 29:this.$ = odd($$[$0]);
break;
case 30:this.$ = $$[$0];
break;
case 31:this.$ = ($$[$0-2] == $$[$0-1]) ? 1 : 0;
break;
case 32:this.$ = ($$[$0-2] != $$[$0-1]) ? 1 : 0;
break;
case 33:this.$ = ($$[$0-2] < $$[$0-1]) ? 1 : 0;
break;
case 34:this.$ = ($$[$0-2] <= $$[$0-1]) ? 1 : 0;
break;
case 35:this.$ = ($$[$0-2] > $$[$0-1]) ? 1 : 0;
break;
case 36:this.$ = ($$[$0-2] >= $$[$0-1]) ? 1 : 0;
break;
case 38:this.$ = ($$[$0-2] == $$[$0-1]) ? 1 : 0;
break;
case 39:this.$ = ($$[$0-2] != $$[$0-1]) ? 1 : 0;
break;
case 40:this.$ = ($$[$0-2] < $$[$0-1]) ? 1 : 0;
break;
case 41:this.$ = ($$[$0-2] <= $$[$0-1]) ? 1 : 0;
break;
case 42:this.$ = ($$[$0-2] > $$[$0-1]) ? 1 : 0;
break;
case 43:this.$ = ($$[$0-2] >= $$[$0-1]) ? 1 : 0;
break;
case 44: this.$ = (typeof $$[$0] === 'undefined')? [] : [ $$[$0] ]; 
break;
case 45: this.$ = $$[$0-2];
          if ($$[$0]) this.$.push($$[$0]); 
          console.log(this.$);
        
break;
case 47:this.$ = $$[$0]
break;
case 48: symbol_table[$$[$0-2]] = this.$ = $$[$0]; 
break;
case 49: throw new Error("Can't assign to constant 'π'"); 
break;
case 50: throw new Error("Can't assign to math constant 'e'"); 
break;
case 51:this.$ = $$[$0-2]+$$[$0];
break;
case 52:this.$ = $$[$0-2]-$$[$0];
break;
case 53:this.$ = $$[$0-2]*$$[$0];
break;
case 54:
          if ($$[$0] == 0) throw new Error("Division by zero, error!");
          this.$ = $$[$0-2]/$$[$0];
        
break;
case 55:this.$ = Math.pow($$[$0-2], $$[$0]);
break;
case 56:
          if ($$[$0-1] % 1 !== 0) 
             throw "Error! Attempt to compute the factorial of "+
                   "a floating point number "+$$[$0-1];
          this.$ = fact($$[$0-1]);
        
break;
case 57:this.$ = $$[$0-1]/100;
break;
case 58:this.$ = -$$[$0];
break;
case 59:this.$ = $$[$0-1];
break;
case 60:this.$ = Number(yytext);
break;
case 61:this.$ = Math.E;
break;
case 62:this.$ = Math.PI;
break;
case 63: this.$ = symbol_table[yytext] || 0; 
break;
}
},
table: [{3:1,4:2,5:[2,2],6:3,7:4,8:5,9:6,10:7,11:[1,10],12:[1,13],13:[2,2],14:[1,8],17:[1,23],19:[1,9],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{1:[3]},{5:[1,24]},{4:25,5:[2,2],6:3,7:4,8:5,9:6,10:7,11:[1,10],12:[1,13],13:[2,2],14:[1,8],17:[1,23],19:[1,9],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{4:26,5:[2,2],6:3,7:4,8:5,9:6,10:7,11:[1,10],12:[1,13],13:[2,2],14:[1,8],17:[1,23],19:[1,9],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{4:27,5:[2,10],6:3,7:4,8:5,9:6,10:7,11:[1,10],12:[1,13],13:[2,10],14:[1,8],17:[1,23],19:[1,9],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{4:28,5:[2,28],6:3,7:4,8:5,9:6,10:7,11:[1,10],12:[1,13],13:[2,28],14:[1,8],17:[1,23],19:[1,9],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{5:[2,7],13:[1,29]},{12:[1,31],15:30},{12:[1,33],20:32},{12:[1,34]},{5:[2,27],11:[2,27],12:[2,27],13:[1,35],14:[2,27],17:[2,27],19:[2,27],22:[2,27],23:[2,27],24:[2,27],25:[2,27],28:[2,27],42:[2,27],43:[2,27],45:[2,27],51:[2,27]},{5:[2,44],11:[2,44],12:[2,44],13:[2,44],14:[2,44],17:[2,44],19:[2,44],22:[2,44],23:[2,44],24:[2,44],25:[2,44],27:[2,44],28:[2,44],33:[2,44],34:[2,44],35:[2,44],36:[2,44],37:[2,44],38:[2,44],42:[2,44],43:[2,44],45:[2,44],51:[2,44]},{5:[2,63],13:[2,63],16:[1,36],44:[2,63],45:[2,63],46:[2,63],47:[2,63],48:[2,63],49:[2,63],50:[2,63]},{12:[1,37]},{9:38,12:[1,39],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17]},{10:43,12:[1,45],13:[2,46],17:[1,23],26:40,31:[1,41],32:42,33:[2,46],34:[2,46],35:[2,46],36:[2,46],37:[2,46],38:[2,46],39:44,40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{29:[1,46]},{5:[2,47],11:[2,47],12:[2,47],13:[2,47],14:[2,47],17:[2,47],19:[2,47],22:[2,47],23:[2,47],24:[2,47],25:[2,47],27:[2,47],28:[2,47],33:[2,47],34:[2,47],35:[2,47],36:[2,47],37:[2,47],38:[2,47],42:[2,47],43:[2,47],44:[1,47],45:[1,48],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],51:[2,47]},{5:[2,62],11:[2,62],12:[2,62],13:[2,62],14:[2,62],16:[1,54],17:[2,62],19:[2,62],22:[2,62],23:[2,62],24:[2,62],25:[2,62],27:[2,62],28:[2,62],33:[2,62],34:[2,62],35:[2,62],36:[2,62],37:[2,62],38:[2,62],42:[2,62],43:[2,62],44:[2,62],45:[2,62],46:[2,62],47:[2,62],48:[2,62],49:[2,62],50:[2,62],51:[2,62],52:[2,62]},{5:[2,61],11:[2,61],12:[2,61],13:[2,61],14:[2,61],16:[1,55],17:[2,61],19:[2,61],22:[2,61],23:[2,61],24:[2,61],25:[2,61],27:[2,61],28:[2,61],33:[2,61],34:[2,61],35:[2,61],36:[2,61],37:[2,61],38:[2,61],42:[2,61],43:[2,61],44:[2,61],45:[2,61],46:[2,61],47:[2,61],48:[2,61],49:[2,61],50:[2,61],51:[2,61],52:[2,61]},{12:[1,57],17:[1,23],41:56,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{12:[1,57],17:[1,23],41:58,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{5:[2,60],11:[2,60],12:[2,60],13:[2,60],14:[2,60],17:[2,60],19:[2,60],22:[2,60],23:[2,60],24:[2,60],25:[2,60],27:[2,60],28:[2,60],33:[2,60],34:[2,60],35:[2,60],36:[2,60],37:[2,60],38:[2,60],42:[2,60],43:[2,60],44:[2,60],45:[2,60],46:[2,60],47:[2,60],48:[2,60],49:[2,60],50:[2,60],51:[2,60],52:[2,60]},{1:[2,1]},{5:[2,3],13:[2,3]},{5:[2,4],13:[2,4]},{5:[2,5],13:[2,5]},{5:[2,6],13:[2,6]},{5:[2,46],11:[2,46],12:[1,57],13:[2,46],14:[2,46],17:[1,23],19:[2,46],22:[2,46],23:[2,46],24:[2,46],25:[2,46],27:[2,46],28:[2,46],33:[2,46],34:[2,46],35:[2,46],36:[2,46],37:[2,46],38:[2,46],40:59,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{13:[1,60]},{16:[1,61]},{13:[1,62]},{13:[2,19],18:[1,63]},{13:[1,64]},{5:[2,26],11:[2,26],12:[2,26],13:[2,26],14:[2,26],17:[2,26],19:[2,26],22:[2,26],23:[2,26],24:[2,26],25:[2,26],28:[2,26],42:[2,26],43:[2,26],45:[2,26],51:[2,26]},{5:[2,46],10:65,11:[2,46],12:[1,57],13:[2,46],14:[2,46],17:[1,23],19:[2,46],22:[2,46],23:[2,46],25:[2,46],28:[2,46],40:12,41:66,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{5:[2,22],11:[2,22],12:[2,22],13:[2,22],14:[2,22],17:[2,22],19:[2,22],22:[2,22],23:[2,22],24:[2,22],25:[2,22],28:[2,22],42:[2,22],43:[2,22],45:[2,22],51:[2,22]},{24:[1,67]},{16:[1,68]},{27:[1,69]},{10:70,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{27:[2,30]},{13:[1,29],33:[1,71],34:[1,72],35:[1,73],36:[1,74],37:[1,75],38:[1,76]},{27:[2,37]},{13:[2,63],16:[1,83],33:[1,77],34:[1,78],35:[1,79],36:[1,80],37:[1,81],38:[1,82],44:[2,63],45:[2,63],46:[2,63],47:[2,63],48:[2,63],49:[2,63],50:[2,63]},{30:[1,84]},{12:[1,57],17:[1,23],41:85,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{12:[1,57],17:[1,23],41:86,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{12:[1,57],17:[1,23],41:87,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{12:[1,57],17:[1,23],41:88,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{12:[1,57],17:[1,23],41:89,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{5:[2,56],11:[2,56],12:[2,56],13:[2,56],14:[2,56],17:[2,56],19:[2,56],22:[2,56],23:[2,56],24:[2,56],25:[2,56],27:[2,56],28:[2,56],33:[2,56],34:[2,56],35:[2,56],36:[2,56],37:[2,56],38:[2,56],42:[2,56],43:[2,56],44:[2,56],45:[2,56],46:[2,56],47:[2,56],48:[2,56],49:[2,56],50:[2,56],51:[2,56],52:[2,56]},{5:[2,57],11:[2,57],12:[2,57],13:[2,57],14:[2,57],17:[2,57],19:[2,57],22:[2,57],23:[2,57],24:[2,57],25:[2,57],27:[2,57],28:[2,57],33:[2,57],34:[2,57],35:[2,57],36:[2,57],37:[2,57],38:[2,57],42:[2,57],43:[2,57],44:[2,57],45:[2,57],46:[2,57],47:[2,57],48:[2,57],49:[2,57],50:[2,57],51:[2,57],52:[2,57]},{12:[1,57],17:[1,23],41:90,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{12:[1,57],17:[1,23],41:91,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{5:[2,58],11:[2,58],12:[2,58],13:[2,58],14:[2,58],17:[2,58],19:[2,58],22:[2,58],23:[2,58],24:[2,58],25:[2,58],27:[2,58],28:[2,58],33:[2,58],34:[2,58],35:[2,58],36:[2,58],37:[2,58],38:[2,58],42:[2,58],43:[2,58],44:[2,58],45:[2,58],46:[2,58],47:[2,58],48:[2,58],49:[1,52],50:[2,58],51:[2,58],52:[2,58]},{5:[2,63],11:[2,63],12:[2,63],13:[2,63],14:[2,63],16:[1,83],17:[2,63],19:[2,63],22:[2,63],23:[2,63],24:[2,63],25:[2,63],27:[2,63],28:[2,63],33:[2,63],34:[2,63],35:[2,63],36:[2,63],37:[2,63],38:[2,63],42:[2,63],43:[2,63],44:[2,63],45:[2,63],46:[2,63],47:[2,63],48:[2,63],49:[2,63],50:[2,63],51:[2,63],52:[2,63]},{44:[1,47],45:[1,48],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],52:[1,92]},{5:[2,45],11:[2,45],12:[2,45],13:[2,45],14:[2,45],17:[2,45],19:[2,45],22:[2,45],23:[2,45],24:[2,45],25:[2,45],27:[2,45],28:[2,45],33:[2,45],34:[2,45],35:[2,45],36:[2,45],37:[2,45],38:[2,45],42:[2,45],43:[2,45],45:[2,45],51:[2,45]},{5:[2,11],6:93,11:[2,11],12:[2,11],13:[2,11],14:[1,8],17:[2,11],19:[2,11],22:[2,11],23:[2,11],25:[2,11],28:[2,11],42:[2,11],43:[2,11],45:[2,11],51:[2,11]},{17:[1,94]},{5:[2,17],11:[2,17],12:[2,17],13:[2,17],14:[2,17],17:[2,17],19:[2,17],22:[2,17],23:[2,17],25:[2,17],28:[2,17],42:[2,17],43:[2,17],45:[2,17],51:[2,17]},{12:[1,33],20:95},{4:96,6:3,7:4,8:5,9:6,10:7,11:[1,10],12:[1,13],13:[2,2],14:[1,8],17:[1,23],19:[1,9],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{5:[2,21],11:[2,21],12:[2,21],13:[1,29],14:[2,21],17:[2,21],19:[2,21],22:[2,21],23:[2,21],24:[2,21],25:[2,21],28:[2,21],42:[2,21],43:[2,21],45:[2,21],51:[2,21]},{5:[2,48],11:[2,47],12:[2,47],13:[2,48],14:[2,47],17:[2,47],19:[2,47],22:[2,47],23:[2,47],25:[2,47],28:[2,47],42:[2,47],43:[2,47],44:[1,47],45:[1,48],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],51:[2,47]},{5:[2,23],11:[2,23],12:[2,23],13:[2,23],14:[2,23],17:[2,23],19:[2,23],22:[2,23],23:[2,23],24:[2,23],25:[2,23],28:[2,23],42:[2,23],43:[2,23],45:[2,23],51:[2,23]},{5:[2,46],10:65,11:[2,46],12:[1,57],13:[2,46],14:[2,46],17:[1,23],19:[2,46],22:[2,46],23:[2,46],24:[2,46],25:[2,46],28:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{9:97,12:[1,39],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17]},{13:[1,29],27:[2,29]},{10:98,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:99,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:100,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:101,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:102,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:103,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:104,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:105,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:106,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:107,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:108,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{10:109,12:[1,57],13:[2,46],17:[1,23],27:[2,46],40:12,41:18,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{12:[1,57],17:[1,23],41:110,42:[1,19],43:[1,20],45:[1,21],51:[1,22]},{9:111,12:[1,39],21:11,22:[1,14],23:[1,15],25:[1,16],28:[1,17]},{5:[2,51],11:[2,51],12:[2,51],13:[2,51],14:[2,51],17:[2,51],19:[2,51],22:[2,51],23:[2,51],24:[2,51],25:[2,51],27:[2,51],28:[2,51],33:[2,51],34:[2,51],35:[2,51],36:[2,51],37:[2,51],38:[2,51],42:[2,51],43:[2,51],44:[2,51],45:[2,51],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],51:[2,51],52:[2,51]},{5:[2,52],11:[2,52],12:[2,52],13:[2,52],14:[2,52],17:[2,52],19:[2,52],22:[2,52],23:[2,52],24:[2,52],25:[2,52],27:[2,52],28:[2,52],33:[2,52],34:[2,52],35:[2,52],36:[2,52],37:[2,52],38:[2,52],42:[2,52],43:[2,52],44:[2,52],45:[2,52],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],51:[2,52],52:[2,52]},{5:[2,53],11:[2,53],12:[2,53],13:[2,53],14:[2,53],17:[2,53],19:[2,53],22:[2,53],23:[2,53],24:[2,53],25:[2,53],27:[2,53],28:[2,53],33:[2,53],34:[2,53],35:[2,53],36:[2,53],37:[2,53],38:[2,53],42:[2,53],43:[2,53],44:[2,53],45:[2,53],46:[2,53],47:[2,53],48:[1,51],49:[1,52],50:[1,53],51:[2,53],52:[2,53]},{5:[2,54],11:[2,54],12:[2,54],13:[2,54],14:[2,54],17:[2,54],19:[2,54],22:[2,54],23:[2,54],24:[2,54],25:[2,54],27:[2,54],28:[2,54],33:[2,54],34:[2,54],35:[2,54],36:[2,54],37:[2,54],38:[2,54],42:[2,54],43:[2,54],44:[2,54],45:[2,54],46:[2,54],47:[2,54],48:[1,51],49:[1,52],50:[1,53],51:[2,54],52:[2,54]},{5:[2,55],11:[2,55],12:[2,55],13:[2,55],14:[2,55],17:[2,55],19:[2,55],22:[2,55],23:[2,55],24:[2,55],25:[2,55],27:[2,55],28:[2,55],33:[2,55],34:[2,55],35:[2,55],36:[2,55],37:[2,55],38:[2,55],42:[2,55],43:[2,55],44:[2,55],45:[2,55],46:[2,55],47:[2,55],48:[2,55],49:[1,52],50:[1,53],51:[2,55],52:[2,55]},{5:[2,49],11:[2,49],12:[2,49],13:[2,49],14:[2,49],17:[2,49],19:[2,49],22:[2,49],23:[2,49],24:[2,49],25:[2,49],27:[2,49],28:[2,49],33:[2,49],34:[2,49],35:[2,49],36:[2,49],37:[2,49],38:[2,49],42:[2,49],43:[2,49],44:[1,47],45:[1,48],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],51:[2,49],52:[2,49]},{5:[2,50],11:[2,50],12:[2,50],13:[2,50],14:[2,50],17:[2,50],19:[2,50],22:[2,50],23:[2,50],24:[2,50],25:[2,50],27:[2,50],28:[2,50],33:[2,50],34:[2,50],35:[2,50],36:[2,50],37:[2,50],38:[2,50],42:[2,50],43:[2,50],44:[1,47],45:[1,48],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],51:[2,50],52:[2,50]},{5:[2,59],11:[2,59],12:[2,59],13:[2,59],14:[2,59],17:[2,59],19:[2,59],22:[2,59],23:[2,59],24:[2,59],25:[2,59],27:[2,59],28:[2,59],33:[2,59],34:[2,59],35:[2,59],36:[2,59],37:[2,59],38:[2,59],42:[2,59],43:[2,59],44:[2,59],45:[2,59],46:[2,59],47:[2,59],48:[2,59],49:[2,59],50:[2,59],51:[2,59],52:[2,59]},{5:[2,12],11:[2,12],12:[2,12],13:[2,12],14:[2,12],17:[2,12],19:[2,12],22:[2,12],23:[2,12],25:[2,12],28:[2,12],42:[2,12],43:[2,12],45:[2,12],51:[2,12]},{13:[2,14],18:[1,112]},{13:[2,18]},{13:[1,113]},{5:[2,24],11:[2,24],12:[2,24],13:[2,24],14:[2,24],17:[2,24],19:[2,24],22:[2,24],23:[2,24],24:[2,24],25:[2,24],28:[2,24],42:[2,24],43:[2,24],45:[2,24],51:[2,24]},{13:[1,29],27:[2,31]},{13:[1,29],27:[2,32]},{13:[1,29],27:[2,33]},{13:[1,29],27:[2,34]},{13:[1,29],27:[2,35]},{13:[1,29],27:[2,36]},{13:[1,29],27:[2,38]},{13:[1,29],27:[2,39]},{13:[1,29],27:[2,40]},{13:[1,29],27:[2,41]},{13:[1,29],27:[2,42]},{13:[1,29],27:[2,43]},{5:[2,48],11:[2,48],12:[2,48],13:[2,48],14:[2,48],17:[2,48],19:[2,48],22:[2,48],23:[2,48],24:[2,48],25:[2,48],27:[2,48],28:[2,48],33:[2,48],34:[2,48],35:[2,48],36:[2,48],37:[2,48],38:[2,48],42:[2,48],43:[2,48],44:[1,47],45:[1,48],46:[1,49],47:[1,50],48:[1,51],49:[1,52],50:[1,53],51:[2,48],52:[2,48]},{5:[2,25],11:[2,25],12:[2,25],13:[2,25],14:[2,25],17:[2,25],19:[2,25],22:[2,25],23:[2,25],24:[2,25],25:[2,25],28:[2,25],42:[2,25],43:[2,25],45:[2,25],51:[2,25]},{12:[1,31],15:114},{5:[2,9],11:[2,9],12:[2,9],13:[2,9],14:[2,9],17:[2,9],19:[2,9],22:[2,9],23:[2,9],25:[2,9],28:[2,9],42:[2,9],43:[2,9],45:[2,9],51:[2,9]},{13:[2,13]}],
defaultActions: {24:[2,1],42:[2,30],44:[2,37],95:[2,18],114:[2,13]},
parseError: function parseError(str, hash) {
    if (hash.recoverable) {
        this.trace(str);
    } else {
        throw new Error(str);
    }
},
parse: function parse(input) {
    var self = this, stack = [0], vstack = [null], lstack = [], table = this.table, yytext = '', yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
    var args = lstack.slice.call(arguments, 1);
    this.lexer.setInput(input);
    this.lexer.yy = this.yy;
    this.yy.lexer = this.lexer;
    this.yy.parser = this;
    if (typeof this.lexer.yylloc == 'undefined') {
        this.lexer.yylloc = {};
    }
    var yyloc = this.lexer.yylloc;
    lstack.push(yyloc);
    var ranges = this.lexer.options && this.lexer.options.ranges;
    if (typeof this.yy.parseError === 'function') {
        this.parseError = this.yy.parseError;
    } else {
        this.parseError = Object.getPrototypeOf(this).parseError;
    }
    function popStack(n) {
        stack.length = stack.length - 2 * n;
        vstack.length = vstack.length - n;
        lstack.length = lstack.length - n;
    }
    function lex() {
        var token;
        token = self.lexer.lex() || EOF;
        if (typeof token !== 'number') {
            token = self.symbols_[token] || token;
        }
        return token;
    }
    var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
    while (true) {
        state = stack[stack.length - 1];
        if (this.defaultActions[state]) {
            action = this.defaultActions[state];
        } else {
            if (symbol === null || typeof symbol == 'undefined') {
                symbol = lex();
            }
            action = table[state] && table[state][symbol];
        }
                    if (typeof action === 'undefined' || !action.length || !action[0]) {
                var errStr = '';
                expected = [];
                for (p in table[state]) {
                    if (this.terminals_[p] && p > TERROR) {
                        expected.push('\'' + this.terminals_[p] + '\'');
                    }
                }
                if (this.lexer.showPosition) {
                    errStr = 'Parse error on line ' + (yylineno + 1) + ':\n' + this.lexer.showPosition() + '\nExpecting ' + expected.join(', ') + ', got \'' + (this.terminals_[symbol] || symbol) + '\'';
                } else {
                    errStr = 'Parse error on line ' + (yylineno + 1) + ': Unexpected ' + (symbol == EOF ? 'end of input' : '\'' + (this.terminals_[symbol] || symbol) + '\'');
                }
                this.parseError(errStr, {
                    text: this.lexer.match,
                    token: this.terminals_[symbol] || symbol,
                    line: this.lexer.yylineno,
                    loc: yyloc,
                    expected: expected
                });
            }
        if (action[0] instanceof Array && action.length > 1) {
            throw new Error('Parse Error: multiple actions possible at state: ' + state + ', token: ' + symbol);
        }
        switch (action[0]) {
        case 1:
            stack.push(symbol);
            vstack.push(this.lexer.yytext);
            lstack.push(this.lexer.yylloc);
            stack.push(action[1]);
            symbol = null;
            if (!preErrorSymbol) {
                yyleng = this.lexer.yyleng;
                yytext = this.lexer.yytext;
                yylineno = this.lexer.yylineno;
                yyloc = this.lexer.yylloc;
                if (recovering > 0) {
                    recovering--;
                }
            } else {
                symbol = preErrorSymbol;
                preErrorSymbol = null;
            }
            break;
        case 2:
            len = this.productions_[action[1]][1];
            yyval.$ = vstack[vstack.length - len];
            yyval._$ = {
                first_line: lstack[lstack.length - (len || 1)].first_line,
                last_line: lstack[lstack.length - 1].last_line,
                first_column: lstack[lstack.length - (len || 1)].first_column,
                last_column: lstack[lstack.length - 1].last_column
            };
            if (ranges) {
                yyval._$.range = [
                    lstack[lstack.length - (len || 1)].range[0],
                    lstack[lstack.length - 1].range[1]
                ];
            }
            r = this.performAction.apply(yyval, [
                yytext,
                yyleng,
                yylineno,
                this.yy,
                action[1],
                vstack,
                lstack
            ].concat(args));
            if (typeof r !== 'undefined') {
                return r;
            }
            if (len) {
                stack = stack.slice(0, -1 * len * 2);
                vstack = vstack.slice(0, -1 * len);
                lstack = lstack.slice(0, -1 * len);
            }
            stack.push(this.productions_[action[1]][0]);
            vstack.push(yyval.$);
            lstack.push(yyval._$);
            newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
            stack.push(newState);
            break;
        case 3:
            return true;
        }
    }
    return true;
}};


var symbol_table = {};

function fact (n){ 
  return n==0 ? 1 : fact(n-1) * n 
}

function odd (n) {
   return (n%2)==0 ? 1 : 0
}

/* generated by jison-lex 0.2.1 */
var lexer = (function(){
var lexer = {

EOF:1,

parseError:function parseError(str, hash) {
        if (this.yy.parser) {
            this.yy.parser.parseError(str, hash);
        } else {
            throw new Error(str);
        }
    },

// resets the lexer, sets new input
setInput:function (input) {
        this._input = input;
        this._more = this._backtrack = this.done = false;
        this.yylineno = this.yyleng = 0;
        this.yytext = this.matched = this.match = '';
        this.conditionStack = ['INITIAL'];
        this.yylloc = {
            first_line: 1,
            first_column: 0,
            last_line: 1,
            last_column: 0
        };
        if (this.options.ranges) {
            this.yylloc.range = [0,0];
        }
        this.offset = 0;
        return this;
    },

// consumes and returns one char from the input
input:function () {
        var ch = this._input[0];
        this.yytext += ch;
        this.yyleng++;
        this.offset++;
        this.match += ch;
        this.matched += ch;
        var lines = ch.match(/(?:\r\n?|\n).*/g);
        if (lines) {
            this.yylineno++;
            this.yylloc.last_line++;
        } else {
            this.yylloc.last_column++;
        }
        if (this.options.ranges) {
            this.yylloc.range[1]++;
        }

        this._input = this._input.slice(1);
        return ch;
    },

// unshifts one char (or a string) into the input
unput:function (ch) {
        var len = ch.length;
        var lines = ch.split(/(?:\r\n?|\n)/g);

        this._input = ch + this._input;
        this.yytext = this.yytext.substr(0, this.yytext.length - len - 1);
        //this.yyleng -= len;
        this.offset -= len;
        var oldLines = this.match.split(/(?:\r\n?|\n)/g);
        this.match = this.match.substr(0, this.match.length - 1);
        this.matched = this.matched.substr(0, this.matched.length - 1);

        if (lines.length - 1) {
            this.yylineno -= lines.length - 1;
        }
        var r = this.yylloc.range;

        this.yylloc = {
            first_line: this.yylloc.first_line,
            last_line: this.yylineno + 1,
            first_column: this.yylloc.first_column,
            last_column: lines ?
                (lines.length === oldLines.length ? this.yylloc.first_column : 0)
                 + oldLines[oldLines.length - lines.length].length - lines[0].length :
              this.yylloc.first_column - len
        };

        if (this.options.ranges) {
            this.yylloc.range = [r[0], r[0] + this.yyleng - len];
        }
        this.yyleng = this.yytext.length;
        return this;
    },

// When called from action, caches matched text and appends it on next action
more:function () {
        this._more = true;
        return this;
    },

// When called from action, signals the lexer that this rule fails to match the input, so the next matching rule (regex) should be tested instead.
reject:function () {
        if (this.options.backtrack_lexer) {
            this._backtrack = true;
        } else {
            return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. You can only invoke reject() in the lexer when the lexer is of the backtracking persuasion (options.backtrack_lexer = true).\n' + this.showPosition(), {
                text: "",
                token: null,
                line: this.yylineno
            });

        }
        return this;
    },

// retain first n characters of the match
less:function (n) {
        this.unput(this.match.slice(n));
    },

// displays already matched input, i.e. for error messages
pastInput:function () {
        var past = this.matched.substr(0, this.matched.length - this.match.length);
        return (past.length > 20 ? '...':'') + past.substr(-20).replace(/\n/g, "");
    },

// displays upcoming input, i.e. for error messages
upcomingInput:function () {
        var next = this.match;
        if (next.length < 20) {
            next += this._input.substr(0, 20-next.length);
        }
        return (next.substr(0,20) + (next.length > 20 ? '...' : '')).replace(/\n/g, "");
    },

// displays the character position where the lexing error occurred, i.e. for error messages
showPosition:function () {
        var pre = this.pastInput();
        var c = new Array(pre.length + 1).join("-");
        return pre + this.upcomingInput() + "\n" + c + "^";
    },

// test the lexed token: return FALSE when not a match, otherwise return token
test_match:function (match, indexed_rule) {
        var token,
            lines,
            backup;

        if (this.options.backtrack_lexer) {
            // save context
            backup = {
                yylineno: this.yylineno,
                yylloc: {
                    first_line: this.yylloc.first_line,
                    last_line: this.last_line,
                    first_column: this.yylloc.first_column,
                    last_column: this.yylloc.last_column
                },
                yytext: this.yytext,
                match: this.match,
                matches: this.matches,
                matched: this.matched,
                yyleng: this.yyleng,
                offset: this.offset,
                _more: this._more,
                _input: this._input,
                yy: this.yy,
                conditionStack: this.conditionStack.slice(0),
                done: this.done
            };
            if (this.options.ranges) {
                backup.yylloc.range = this.yylloc.range.slice(0);
            }
        }

        lines = match[0].match(/(?:\r\n?|\n).*/g);
        if (lines) {
            this.yylineno += lines.length;
        }
        this.yylloc = {
            first_line: this.yylloc.last_line,
            last_line: this.yylineno + 1,
            first_column: this.yylloc.last_column,
            last_column: lines ?
                         lines[lines.length - 1].length - lines[lines.length - 1].match(/\r?\n?/)[0].length :
                         this.yylloc.last_column + match[0].length
        };
        this.yytext += match[0];
        this.match += match[0];
        this.matches = match;
        this.yyleng = this.yytext.length;
        if (this.options.ranges) {
            this.yylloc.range = [this.offset, this.offset += this.yyleng];
        }
        this._more = false;
        this._backtrack = false;
        this._input = this._input.slice(match[0].length);
        this.matched += match[0];
        token = this.performAction.call(this, this.yy, this, indexed_rule, this.conditionStack[this.conditionStack.length - 1]);
        if (this.done && this._input) {
            this.done = false;
        }
        if (token) {
            return token;
        } else if (this._backtrack) {
            // recover context
            for (var k in backup) {
                this[k] = backup[k];
            }
            return false; // rule action called reject() implying the next rule should be tested instead.
        }
        return false;
    },

// return next match in input
next:function () {
        if (this.done) {
            return this.EOF;
        }
        if (!this._input) {
            this.done = true;
        }

        var token,
            match,
            tempMatch,
            index;
        if (!this._more) {
            this.yytext = '';
            this.match = '';
        }
        var rules = this._currentRules();
        for (var i = 0; i < rules.length; i++) {
            tempMatch = this._input.match(this.rules[rules[i]]);
            if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
                match = tempMatch;
                index = i;
                if (this.options.backtrack_lexer) {
                    token = this.test_match(tempMatch, rules[i]);
                    if (token !== false) {
                        return token;
                    } else if (this._backtrack) {
                        match = false;
                        continue; // rule action called reject() implying a rule MISmatch.
                    } else {
                        // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                        return false;
                    }
                } else if (!this.options.flex) {
                    break;
                }
            }
        }
        if (match) {
            token = this.test_match(match, rules[index]);
            if (token !== false) {
                return token;
            }
            // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
            return false;
        }
        if (this._input === "") {
            return this.EOF;
        } else {
            return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. Unrecognized text.\n' + this.showPosition(), {
                text: "",
                token: null,
                line: this.yylineno
            });
        }
    },

// return next match that has a token
lex:function lex() {
        var r = this.next();
        if (r) {
            return r;
        } else {
            return this.lex();
        }
    },

// activates a new lexer condition state (pushes the new lexer condition state onto the condition stack)
begin:function begin(condition) {
        this.conditionStack.push(condition);
    },

// pop the previously active lexer condition state off the condition stack
popState:function popState() {
        var n = this.conditionStack.length - 1;
        if (n > 0) {
            return this.conditionStack.pop();
        } else {
            return this.conditionStack[0];
        }
    },

// produce the lexer rule set which is active for the currently active lexer condition state
_currentRules:function _currentRules() {
        if (this.conditionStack.length && this.conditionStack[this.conditionStack.length - 1]) {
            return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules;
        } else {
            return this.conditions["INITIAL"].rules;
        }
    },

// return the currently active lexer condition state; when an index argument is provided it produces the N-th previous condition state, if available
topState:function topState(n) {
        n = this.conditionStack.length - 1 - Math.abs(n || 0);
        if (n >= 0) {
            return this.conditionStack[n];
        } else {
            return "INITIAL";
        }
    },

// alias for begin(condition)
pushState:function pushState(condition) {
        this.begin(condition);
    },

// return the number of states currently on the stack
stateStackSize:function stateStackSize() {
        return this.conditionStack.length;
    },
options: {},
performAction: function anonymous(yy,yy_,$avoiding_name_collisions,YY_START) {

var reserved_words ={ PI: 'PI', E : 'E', CONST: 'const', VAR: 'var', PROCEDURE: 'procedure',
CALL: 'call', BEGIN: 'begin', END: 'end', IF: 'if', THEN: 'then', WHILE: 'while', DO: 'do',
ODD: 'odd'}

function idORrw(x) {
  return (x.toUpperCase() in reserved_words)? x.toUpperCase() : 'ID'
}


var YYSTATE=YY_START;
switch($avoiding_name_collisions) {
case 0:/* skip whitespace and comments */
break;
case 1:return 17
break;
case 2:return idORrw(yy_.yytext)
break;
case 3:return yy_.yytext;
break;
case 4:return 5
break;
case 5:return 'INVALID'
break;
}
},
rules: [/^(?:\s+|#.*)/,/^(?:\b\d+(\.\d*)?([eE][-+]?\d+)?\b)/,/^(?:\b[A-Za-z_]\w*\b)/,/^(?:[-*/+^!%=();,])/,/^(?:$)/,/^(?:.)/],
conditions: {"INITIAL":{"rules":[0,1,2,3,4,5],"inclusive":true}}
};
return lexer;
})();
parser.lexer = lexer;
function Parser () {
  this.yy = {};
}
Parser.prototype = parser;parser.Parser = Parser;
return new Parser;
})();


if (typeof require !== 'undefined' && typeof exports !== 'undefined') {
exports.parser = calculator;
exports.Parser = calculator.Parser;
exports.parse = function () { return calculator.parse.apply(calculator, arguments); };
exports.main = function commonjsMain(args) {
    if (!args[1]) {
        console.log('Usage: '+args[0]+' FILE');
        process.exit(1);
    }
    var source = require('fs').readFileSync(require('path').normalize(args[1]), "utf8");
    return exports.parser.parse(source);
};
if (typeof module !== 'undefined' && require.main === module) {
  exports.main(process.argv.slice(1));
}
}