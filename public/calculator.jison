/* description: Parses end executes mathematical expressions. */

%{

var symbol_table = {};

function fact (n){ 
  return n==0 ? 1 : fact(n-1) * n 
}

function odd (n) {
   return (n%2)==0 ? 1 : 0
}

%}

%token PROCEDURE CALL BEGIN END IF THEN WHILE DO
%token ID E PI EOF CONST VAR NUMBER

/* operator associations and precedence */
%right '='
%left '+' '-'
%left '*' '/'
%left '^'
%right '%'
%left UMINUS
%left '!'

%start program

%% /* language grammar */

program
    : block EOF
        { 
          $$ = $1; 
          console.log($$);
          return [$$, symbol_table];
        }
    ;

block
    : /* empty */
    | cons block //¿statement en vez de block?
         {
            if ($2) $$ = [$1, $2];
            else $$ = $1;
            //$$ = $1;
            //$$.push($2);
         }
    | vari  block //¿statement en vez de block?
         {
            if ($2) $$ = [$1, $2];  //$$ = $1; $$.push($2)
            else $$ = $1;
         }
    | proc block
         {
            if ($2) $$ = [$1, $2];  //$$ = $1; $$.push($2)
            else $$ = $1;
         }
    | statements block
         {
            if ($2) $$ = [$1, $2];  //$$ = $1; $$.push($2)
            else $$ = $1;
         }
    | expressions  {$$ = $1;}
    ;

proc
    : /* empty */
    | PROCEDURE ID ';' block ';' {$$ = [$2, $4]} //$$ = $2; $$.push($4);
    | proc {$$ = $1}
    ;
    
cons // CONST P=2;
    : /* empty */
    | CONST constlist ';' cons
         {
            if ($4) $$ = [$2, $4]; //$$ = $2; $$.push($4)
            else $$ = $2;
         }
    ;

constlist // ¿CONST P=2+5, PG=3+7;?
    : ID '=' NUMBER ',' constlist //CONST P=2, PG=3;
         {
            if ($5) $$ = [$3, $5]; //$$ = $3; $$.push($5)
            else $$ = $3;
            symbol_table[$1] = Number($3);
         }
    | ID '=' NUMBER  //CONST P=2;
         {
            $$ = $3;
            symbol_table[$1] = Number($3);
         }
    | constlist {$$ = $1;}
    ;

vari // VAR a;
    : /* empty */
    | VAR varlist ';'  {$$ = $2;}
    ;

varlist // VAR a, b, c;
    : ID ',' varlist
       {
          if ($3) $$ = [$1, $3]; //$$ = $1; $$.push($3)
          else $$ = $1;
       }
    | ID      {$$ = $1;}
    | varlist {$$ = $1;}
    ;

statement
    : ID '=' expressions
          {
             $$ = [$1, $3];
             symbol_table[$1] = $3;
          }
    | CALL ID                      {$$ = $1; symbol_table[$1] = $2}
    | BEGIN statements END          {$$ = $1; symbol_table[$1] = $2}
    | IF condition THEN statements  //$$ = $3; $$.push($4)
         {
            $$ = [$2, $4];
            symbol_table[$1] = $2;
            symbol_table[$3] = $4;
         }
    | WHILE codition DO statements  //$$ = $2; $$.push($4)
         {
            $$ = [$2, $4];
            symbol_table[$1] = $2;
            symbol_table[$3] = $4;
         }
    ;

statements
    : statement ';'  {$$ = $1;}
    | statement      {$$ = $1;}
    | statements     {$$ = $1;}
    ;

condition
    : ODD expressions {$$ = odd($2);}
    | comparision {$$ = $1;}
    ; 

comparision
    : expressions '==' expressions {$$ = ($1 == $2) ? 1 : 0;}
    | expressions '#' expressions  {$$ = ($1 != $2) ? 1 : 0;}
    | expressions '<' expressions  {$$ = ($1 < $2) ? 1 : 0;}
    | expressions '<=' expressions {$$ = ($1 <= $2) ? 1 : 0;}
    | expressions '>' expressions  {$$ = ($1 > $2) ? 1 : 0;}
    | expressions '>=' expressions {$$ = ($1 >= $2) ? 1 : 0;}
    | compidexp
    ;

compidexp
    : ID '==' expressions {$$ = ($1 == $2) ? 1 : 0;}
    | ID '#' expressions  {$$ = ($1 != $2) ? 1 : 0;}
    | ID '<' expressions  {$$ = ($1 < $2) ? 1 : 0;}
    | ID '<=' expressions {$$ = ($1 <= $2) ? 1 : 0;}
    | ID '>' expressions  {$$ = ($1 > $2) ? 1 : 0;}
    | ID '>=' expressions {$$ = ($1 >= $2) ? 1 : 0;}
    ;
    
expressions
    : s  
        { $$ = (typeof $1 === 'undefined')? [] : [ $1 ]; }
    | expressions ';' s
        { $$ = $1;
          if ($3) $$.push($3); 
          console.log($$);
        }
    ;

s
    : /* empty */
    | e {$$ = $1}
    ;

e
    : ID '=' e
        { symbol_table[$1] = $$ = $3; }        
    | PI '=' e 
        { throw new Error("Can't assign to constant 'π'"); }
    | E '=' e 
        { throw new Error("Can't assign to math constant 'e'"); }
    | e '+' e
        {$$ = $1+$3;}
    | e '-' e
        {$$ = $1-$3;}
    | e '*' e
        {$$ = $1*$3;}
    | e '/' e
        {
          if ($3 == 0) throw new Error("Division by zero, error!");
          $$ = $1/$3;
        }
    | e '^' e
        {$$ = Math.pow($1, $3);}
    | e '!'
        {
          if ($1 % 1 !== 0) 
             throw "Error! Attempt to compute the factorial of "+
                   "a floating point number "+$1;
          $$ = fact($1);
        }
    | e '%'
        {$$ = $1/100;}
    | '-' e %prec UMINUS
        {$$ = -$2;}
    | '(' e ')'
        {$$ = $2;}
    | NUMBER
        {$$ = Number(yytext);} 
    | E
        {$$ = Math.E;}
    | PI
        {$$ = Math.PI;}        
    | ID 
        { $$ = symbol_table[yytext] || 0; }        
    ;