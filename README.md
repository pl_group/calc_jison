#Simple example of how to use Jison in the browser.
=========================================================

* **Universidad:** La Laguna
* **Facultad:** ETSII
* **Carrera:** Grado de Informatica
* **Asignatura:** Procesadores de Lenguajes
* **Profesor:** Casiano Rodriguez
* **Práctica:** Parsin con jison en JavaScript
* **Autor:** Jorge O. Blanchard Cruz y Gonzalo J. García Martin
* **Fork:** [Casiano Rodriguez](https://github.com/crguezl/prdcalc)
* **Version:** 3.5.2

Practica de Parsing con jison en JavaScript, es la sexta práctica de la asignatura de procesadores del lenguaje

Comprueba su funcionamiento en [Heroku](http://calcjison.herokuapp.com/)
Y el repositorio en [Bitbucket](https://alu0100403619@bitbucket.org/pl_group/calc_jison.git)

# Sinatra example Using PEG.js and DataMapper

* [PEG.js](http://pegjs.majda.cz/)
* [DataMapper](http://datamapper.org/docs/)
* [PL0 Grammar. Wikipedia](http://en.wikipedia.org/wiki/Recursive_descent_parser)
* [ULL-ETSII-GRADO-PL-13-14](https://plus.google.com/u/0/communities/107031495100582318205)
* [PL Entorno virtual de docencia institucional 2013/2014 ULL](http://campusvirtual.ull.es/1314/course/view.php?id=1104)

# Ejecucion 

* run `rake` to produce "calculator.js"
* [repo](https://github.com/crguezl/ull-etsii-grado-pl-jisoncalc)
* [Jison](http://zaach.github.io/jison/)

# Problemas:
*Al hacer bundle install dice que no se puede instalar do_sqlit3, la solucion es:
   sudo apt-get install sqlite3
   sudo apt-get install libsqlite3
   
   Fecha: 10/04/2014